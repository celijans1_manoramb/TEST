$(function () {
    console.log("Hello from jQuery");

    var $counters = $("[data-counter-start][data-counter-increment]")
    $counters.each(function ()  {
        var $counter = $(this);
        var count = $counter.data("counter-start");
        var increment = $counter.data("counter-increment")

        $counter
            .css ({
                fontFamily: "Avenir Next"
            })
            .text(count.toLocaleString());
        
            setInterval(function () {
            count = count + increment;
            $counter.text(count.toLocaleString());
        }, 100)

        var $scrollToTopButton = $("#scroll-to-top");
        var $body = $("body");
        var scrollTreshold = window.innerHeight /4;
        
        $(window)
            .on("scroll",function () {
                if ($body.scrollTop() > scrollTreshold) {
                    $scrollToTopButton.show(); }
                else {$scrollToTopButton.hide();}
            });

        $scrollToTopButton.on("click", function () {
            $("body").animate ({
                scrollTop: 0
            })
        });
    });

        var $slidesButtons = $(".slides-button");
        var $slidesItems = $(".slides-item");
        var $screenItems = $(".screen-item");

        $slidesButtons
            .on("click", function() {
                var $activeSlidesButton = $(this);
                var index = $activeSlidesButton.index();

                $slidesButtons
                    .addClass("btn-secondary")
                    .removeClass("btn-default");

                $activeSlidesButton 
                    .addClass("btn-default")
                    .removeClass("btn-secondary");

                $slidesItems
                    .hide()
                    .eq(index).show()

                $screenItems
                    .hide()
                    .eq(index).show()
            })
            .first().trigger("click")
});